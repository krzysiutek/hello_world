#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include <math.h>

#define CZAS 1024
#define KWADRATOWA 1025
#define ENDIANS 1026
#define PORT 2001


int convertEndian(int);

int main ()
{	
	time_t t;
        time(&t);

	int server_sockfd, client_sockfd;
	socklen_t server_len, client_len;
	struct sockaddr_in server_address;
	struct sockaddr_in client_address;

	server_sockfd = socket (AF_INET, SOCK_STREAM, 0);

	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl (INADDR_ANY);
	server_address.sin_port = htons (PORT);
	server_len = sizeof (server_address);
	bind (server_sockfd, (struct sockaddr *) &server_address, server_len);

	/*  Create a connection queue, ignore child exit details and wait for clients.  */

	listen (server_sockfd, 5);

	signal (SIGCHLD, SIG_IGN);

	while (1)
	{
		int ch,a,b,c,endian;

		printf ("Czekam...\n");

		//  Accept

		client_len = sizeof (client_address);
		client_sockfd = accept (server_sockfd,
				(struct sockaddr *) &client_address,
				&client_len);

		

		if (!fork())
		{

			//  Czytanie/pisanie  
			while(1){
				
				if((read(client_sockfd, &ch,sizeof(ch)))==-1){
					perror("Odbior");
             				exit(1);
				}	
				if(ch == CZAS){
					printf("Otrzymano: %d",ch);
					puts("");

					write(client_sockfd, ctime(&t), 24);
					
				}else if(ch == KWADRATOWA){
					printf("Otrzymano: %d",ch);
					puts("");
					if((read(client_sockfd, &a,sizeof(a)))==-1){
						perror("Odbior a");
	             				exit(1);
					}
					if((read(client_sockfd, &b,sizeof(b)))==-1){
						perror("Odbior b");
	             				exit(1);
					}
					if((read(client_sockfd, &c,sizeof(c)))==-1){
						perror("Odbior c");
	             				exit(1);
					}
					
					
					int ile;
					float delta,x0,x1,x2;

					delta = ((float)b*(float)b) - (4*(float)a*(float)c);
					if(delta > 0){
						ile=2;
						write(client_sockfd, &ile, sizeof(ile));


						x1=(((float)b*(-1))-sqrt(delta))/(2*(float)a);
						write(client_sockfd, &x1, sizeof(x1));

						x2=(((float)b*(-1))+sqrt(delta))/(2*(float)a);
						write(client_sockfd, &x2, sizeof(x2));


					}
					else if(delta == 0){
						ile=1;
						write(client_sockfd, &ile, sizeof(ile));
						x0=((float)b*(-1))/(2*(float)a);
						write(client_sockfd, &x0, sizeof(x0));
						
					}
					else {
						ile=0;
						write(client_sockfd, &ile, sizeof(ile));
						
					}
				}else if(ch==ENDIANS){
					printf("Otrzymano: %d",ch);
					puts("");
					if((read(client_sockfd, &endian,sizeof(endian)))==-1){
						perror("Odbior Endian");
	             				exit(1);
					}	

					printf("%d",endian);
					puts("");
					
					int converted = convertEndian(endian);
					printf("%d",converted);	
					puts("");	
					
					int converted2 = convertEndian(converted);
					printf("%d",converted2);	
					puts("");
					write(client_sockfd, &converted2, sizeof(converted2));	
				}
			}
			
			
		}

		else
		{
			close (client_sockfd);
		}
	}
}

int convertEndian(int e){
  //bardziej znaczace zapisuja sie na koncu to jest po prawej - Little Endian

	int i=1,new;
	char * c = (char *) &i;

	if(c[0] == 1){
		
		char * c = (char *)&e;
		char * newOrder = (char *)&new;
		
		newOrder[0] = c[3];
		newOrder[1] = c[2];
		newOrder[2] = c[1];
		newOrder[3] = c[0];
		e = new;
	}

	return e;		   	
}








