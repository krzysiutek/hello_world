#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>

#define CZAS 1024
#define KWADRATOWA 1025
#define ENDIANS 1026
#define KONIEC 0
#define PORT 2001

int convertEndian(int);

int main ()
{
	int sockfd;
	socklen_t len;
	struct sockaddr_in address;
	int result;
	int ch,a,b,c;

	//  Create a socket for the client.

	sockfd = socket (AF_INET, SOCK_STREAM, 0);

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr ("127.0.0.1");
	address.sin_port = htons (PORT);
	len = sizeof (address);

	// connect serversocket - clientsocket

	result = connect (sockfd, (struct sockaddr *) &address, len);

	if (result == -1)
	{
		perror ("Connection");
		exit (0);
	}

	//Czytanie/pisanie
	while(1){

		printf("\nPodaj polecenie \n %d - czas z serwera;\n %d - pierwiastki równania kwadratowego;\n %d - Konwersja Endian\n %d - Zakończ: \n",CZAS,KWADRATOWA,ENDIANS,KONIEC);
		scanf("%d",&ch);

		write(sockfd, &ch, sizeof(ch));

		if(ch == CZAS){
			char czas[24]="";
			read(sockfd, &czas,24);
			printf ("Czas na serwerze: %s\n", czas);
		}else if(ch == KWADRATOWA){
				
			printf("Podaj a: ");
			scanf("%d",&a);
			write(sockfd, &a, sizeof(a));

			printf("Podaj b: ");
			scanf("%d",&b);
			write(sockfd, &b, sizeof(b));

			printf("podaj c: ");
			scanf("%d",&c);
			write(sockfd, &c, sizeof(c));
				
			int ile;
			float x0,x1,x2;
			read(sockfd, &ile,sizeof(ile));
				
			if(ile == 0){
				printf("Równanie nie ma rozwiązań\n");
			}else if(ile == 1){
				read(sockfd, &x0,sizeof(x0));
				printf("Rozwiązanie równania: x0 = %f \n",x0);			
			}else if(ile == 2){
				read(sockfd, &x1,sizeof(x1));
				read(sockfd, &x2,sizeof(x2));
				printf("Pierwiastki równania: x1 = %f : x2 = %f \n",x1,x2);			
			}else{
				printf("Coś poszło nie tak...\n");
			}
		}else if(ch==ENDIANS){
			int endian;
			printf("Podaj liczbe calkowita: ");
			scanf("%d",&endian);
			int converted = convertEndian(endian);
			printf("Przekonwertowana: %d\n", converted);
			write(sockfd,&converted,sizeof(converted));
			
			read(sockfd, &endian, sizeof(endian));
			printf("Od Serwera: %d\n",endian);
			
		}else if(ch==KONIEC){
			printf("Zakończono...\n");
			exit(0);
		}else{
			printf("Błędne polecenie\n");			
		}
	}
	
	close (sockfd);
	exit (0);
}

int convertEndian(int e){
  //bardziej znaczace zapisuja sie na koncu to jest po prawej - Little Endian

	int i=1,new;
	char * c = (char *) &i;

	if(c[0] == 1){
		// konwert na Big Endian
		char * c = (char *)&e;
		char * newOrder = (char *)&new;
		
		newOrder[0] = c[3];
		newOrder[1] = c[2];
		newOrder[2] = c[1];
		newOrder[3] = c[0];
		e = new;
	}

	return e;		   	
}
